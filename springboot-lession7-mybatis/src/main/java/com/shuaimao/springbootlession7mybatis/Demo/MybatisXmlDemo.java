package com.shuaimao.springbootlession7mybatis.Demo;

import com.shuaimao.springbootlession7mybatis.Demo.annotation.UserMapper;
import com.shuaimao.springbootlession7mybatis.Demo.mysql.User;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Properties;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 *
 * @Description
 * @Author: shuaimao
 * @Date: Created In 10:14 2018/3/22
 */

public class MybatisXmlDemo {

    public static void main(String[] args) throws Exception{

        ResourceLoader resourceLoader = new DefaultResourceLoader();

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        InputStream resource = classLoader.getResourceAsStream("mybatis/mybatis-config.xml");

        Reader reader = new InputStreamReader(resource, "UTF-8");

        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();

        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(reader, "dev", new Properties());

        SqlSession sqlSession = sqlSessionFactory.openSession();

        /*User user = sqlSession.selectOne("com.shuaimao.springbootlession7mybatis.Demo.Mapper.UserMapper.selectOneUser", 10);*/
        /*System.out.println(user);*/

        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        User user = userMapper.selectUser(1);


        sqlSession.close();
    }




}
