package com.shuaimao.springbootlession7mybatis.Demo.annotation;

import com.shuaimao.springbootlession7mybatis.Demo.mysql.User;
import org.apache.ibatis.annotations.*;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 *
 * @Description 注解方式写mapper
 * @Author: shuaimao
 * @Date: Created In 15:50 2018/3/22
 */
@Mapper
public interface UserMapper {

    //定义结果集
    @Results(id = "UserResultMap2", value = {
            @Result(property = "id", column = "id", id = true),
            @Result(property = "name", column = "name", id = true),
            @Result(property = "age", column = "age", id = true),
    })


    @Select("SELECT * FROM USER WHERE id = #{id, jdbcType=INTEGER}")
    User selectUser(Integer id);

}
