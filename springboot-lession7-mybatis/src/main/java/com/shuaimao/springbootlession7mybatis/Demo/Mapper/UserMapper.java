package com.shuaimao.springbootlession7mybatis.Demo.Mapper;

import com.shuaimao.springbootlession7mybatis.Demo.mysql.User;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 *
 * @Description
 * @Author: shuaimao
 * @Date: Created In 13:19 2018/3/22
 */
public interface UserMapper {

    public User selectOneUser(Integer id);


}

