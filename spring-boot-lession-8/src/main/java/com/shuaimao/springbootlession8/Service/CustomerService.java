package com.shuaimao.springbootlession8.Service;

import com.shuaimao.springbootlession8.entity.Customer;
import org.springframework.stereotype.Service;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 *
 * @Description 客户服务
 * @Author: shuaimao
 * @Date: Created In 15:25 2018/3/25
 */
@Service
public class CustomerService {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * @Description 添加客户
     * @Author: shuaimao
     * @Date: Created In 15:30 2018/3/25
     */
    @Transactional
    public void addCustomer(Customer customer) {

        entityManager.persist(customer);

    }


    public Customer getCustomerById(Long id) {

        return entityManager.find(Customer.class, id);

    }


/*,
	"creditCard":{
		"number":123456,
		"registeredDate":"2018-03-25"

	}*/


}
