package com.shuaimao.springbootlession8.Controller;

import com.shuaimao.springbootlession8.Service.CustomerService;
import com.shuaimao.springbootlession8.entity.Customer;
import com.shuaimao.springbootlession8.entity.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 *
 * @Description 客户控制器
 * @Author: shuaimao
 * @Date: Created In 15:34 2018/3/25
 */
@RestController
@RequestMapping("customers")
public class CustomerController {


    @Autowired
    CustomerService customerService;

    @Autowired
    CustomerRepository customerRepository;

    /**
     * @Description 添加客户
     * @Author: shuaimao
     * @Date: Created In 15:36 2018/3/25
     */
    @RequestMapping(value = "add", method = RequestMethod.POST)
    public Customer addCustomer(@RequestBody Customer customer) {

        customerService.addCustomer(customer);
        return customerService.getCustomerById(customer.getId());

    }

    @RequestMapping(value = "findAll")
    public List<Customer> loadCustomers() {
        return customerRepository.findAll();
    }

/*
    @RequestMapping("get/{id}")
    public Customer getCustomer(@PathVariable("id") Long id) {
       // return customerRepository.findOne(id);
    }*/

}
