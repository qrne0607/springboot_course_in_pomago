package com.shuaimao.springbootlession8.entity.repository;

import com.shuaimao.springbootlession8.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;


/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 *
 * @Description 用户repository,相当于DAO
 * @Author: shuaimao
 * @Date: Created In 17:03 2018/3/25
 */
@Repository
@Transactional(readOnly = false)
public class CustomerRepository extends SimpleJpaRepository<Customer, Long>{

    @Autowired
    public CustomerRepository(EntityManager em) {
        super(Customer.class, em);
    }






}
