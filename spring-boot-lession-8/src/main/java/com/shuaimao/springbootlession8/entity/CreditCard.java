package com.shuaimao.springbootlession8.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 *
 * @Description 信用卡
 * @Author: shuaimao
 * @Date: Created In 14:04 2018/3/25
 */
@Entity
@Table(name = "credit_cards")
public class CreditCard {

    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 128)
    private String number;

    @Column(name = "reg_date")
    private Date registeredDate;

    @OneToOne(mappedBy = "creditCard")//从，要写上mappedby即IOC的控制人, 即主中所关联属性的名称; CascadeType.REMOVE, 意思是当主表中的记录移除之后，这个表对应的记录也移除
    private Customer customer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(Date registeredDate) {
        this.registeredDate = registeredDate;
    }
}
