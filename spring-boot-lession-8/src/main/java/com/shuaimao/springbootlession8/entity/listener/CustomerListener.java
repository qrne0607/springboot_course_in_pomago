package com.shuaimao.springbootlession8.entity.listener;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 *
 * @Description
 * @Author: shuaimao
 * @Date: Created In 16:24 2018/3/25
 */
public class CustomerListener {

    @PrePersist
    public void prePersist(Object source) {
        System.out.println("pre persist" + source);
    }

    @PostPersist
    public void postPersist(Object source) {
        System.out.println("post persist" + source);
    }



}
