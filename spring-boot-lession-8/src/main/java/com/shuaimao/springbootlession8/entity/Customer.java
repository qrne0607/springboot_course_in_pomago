package com.shuaimao.springbootlession8.entity;


import com.shuaimao.springbootlession8.entity.listener.CustomerListener;

import javax.persistence.*;
import java.util.Collection;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 *
 * @Description 客户实体
 * @Author: shuaimao
 * @Date: Created In 13:28 2018/3/25
 */
@Entity
@Access(value = AccessType.FIELD)
@Table(name = "customers")
@EntityListeners(value = CustomerListener.class)//监听器
public class Customer {

    @Id//主键
    @GeneratedValue()//自增
    private Long id;

    @Column(length = 64)
    private String name;

    @OneToOne(cascade = CascadeType.REMOVE)//主, CascadeType.REMOVE, 意思是当主表中的记录移除之后，其余关联表对应的记录也移除
    private CreditCard creditCard;

    @ManyToOne
    private Store store;

    @ManyToMany
    private Collection<Book> books;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Collection<Book> getBooks() {
        return books;
    }

    public void setBooks(Collection<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", creditCard=" + creditCard +
                ", store=" + store +
                ", books=" + books +
                '}';
    }
}
